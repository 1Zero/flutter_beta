import 'package:flutter/material.dart';
import 'package:componentes1/src/pages/home.dart';

//import 'package:componentes1/src/pages/home_temp.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'componentes1',
      debugShowCheckedModeBanner: false,
      home: HomePages(),
    );
  }
}
